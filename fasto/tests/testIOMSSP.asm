	.text	0x00400000
	.globl	main
	la	$28, _heap_
	la	$4, ___str___217_
# was:	la	___str___217__addr, ___str___217_
	ori	$3, $0, 1
# was:	ori	___str___217__init, 0, 1
	sw	$3, 0($4)
# was:	sw	___str___217__init, 0(___str___217__addr)
	la	$4, _MSSPRes_206_
# was:	la	_MSSPRes_206__addr, _MSSPRes_206_
	ori	$3, $0, 18
# was:	ori	_MSSPRes_206__init, 0, 18
	sw	$3, 0($4)
# was:	sw	_MSSPRes_206__init, 0(_MSSPRes_206__addr)
	la	$4, ___str___55_
# was:	la	___str___55__addr, ___str___55_
	ori	$3, $0, 3
# was:	ori	___str___55__init, 0, 3
	sw	$3, 0($4)
# was:	sw	___str___55__init, 0(___str___55__addr)
	la	$4, ___str___40_
# was:	la	___str___40__addr, ___str___40_
	ori	$3, $0, 3
# was:	ori	___str___40__init, 0, 3
	sw	$3, 0($4)
# was:	sw	___str___40__init, 0(___str___40__addr)
	la	$4, ___str___10_
# was:	la	___str___10__addr, ___str___10_
	ori	$3, $0, 2
# was:	ori	___str___10__init, 0, 2
	sw	$3, 0($4)
# was:	sw	___str___10__init, 0(___str___10__addr)
	la	$4, _Introdu_5_
# was:	la	_Introdu_5__addr, _Introdu_5_
	ori	$3, $0, 24
# was:	ori	_Introdu_5__init, 0, 24
	sw	$3, 0($4)
# was:	sw	_Introdu_5__init, 0(_Introdu_5__addr)
	la	$4, _true
# was:	la	_true_addr, _true
	ori	$3, $0, 4
# was:	ori	_true_init, 0, 4
	sw	$3, 0($4)
# was:	sw	_true_init, 0(_true_addr)
	la	$3, _false
# was:	la	_false_addr, _false
	ori	$4, $0, 5
# was:	ori	_false_init, 0, 5
	sw	$4, 0($3)
# was:	sw	_false_init, 0(_false_addr)
	jal	main
_stop_:
	ori	$2, $0, 10
	syscall
# Function readInt
readInt:
	sw	$31, -4($29)
	sw	$16, -8($29)
	addi	$29, $29, -12
	ori	$16, $2, 0
# was:	ori	_param_i_1_, 2, 0
	la	$2, _Introdu_5_
# was:	la	_tmp_4_, _Introdu_5_
# _Introdu_5_: string "Introduce (Next) Number "
# 	ori	_letBind_3_,_tmp_4_,0
# 	ori	2,_tmp_4_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	_tmp_7_, _param_i_1_, 0
# 	ori	_letBind_6_,_tmp_7_,0
# 	ori	2,_letBind_6_,0
	jal	putint
# was:	jal	putint, 2
	la	$2, ___str___10_
# was:	la	_tmp_9_, ___str___10_
# ___str___10_: string ": "
# 	ori	_letBind_8_,_tmp_9_,0
# 	ori	2,_tmp_9_,0
	jal	putstring
# was:	jal	putstring, 2
	jal	getint
# was:	jal	getint, 2
# 	ori	_readIntres_2_,2,0
# 	ori	2,_readIntres_2_,0
	addi	$29, $29, 12
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function readIntArr
readIntArr:
	sw	$31, -4($29)
	sw	$20, -24($29)
	sw	$19, -20($29)
	sw	$18, -16($29)
	sw	$17, -12($29)
	sw	$16, -8($29)
	addi	$29, $29, -28
# 	ori	_param_n_11_,2,0
# 	ori	_size_reg_14_,_param_n_11_,0
	addi	$2, $2, -1
# was:	addi	_size_reg_14_, _size_reg_14_, -1
	bgez	$2, _safe_lab_15_
# was:	bgez	_size_reg_14_, _safe_lab_15_
	ori	$5, $0, 7
# was:	ori	5, 0, 7
	j	_IllegalArrSizeError_
_safe_lab_15_:
	addi	$2, $2, 1
# was:	addi	_size_reg_14_, _size_reg_14_, 1
	ori	$3, $28, 0
# was:	ori	_letBind_13_, 28, 0
	sll	$4, $2, 2
# was:	sll	_tmp_21_, _size_reg_14_, 2
	addi	$4, $4, 4
# was:	addi	_tmp_21_, _tmp_21_, 4
	add	$28, $28, $4
# was:	add	28, 28, _tmp_21_
	sw	$2, 0($3)
# was:	sw	_size_reg_14_, 0(_letBind_13_)
	addi	$4, $3, 4
# was:	addi	_addr_reg_16_, _letBind_13_, 4
	ori	$5, $0, 0
# was:	ori	_i_reg_17_, 0, 0
_loop_beg_18_:
	sub	$6, $5, $2
# was:	sub	_tmp_reg_20_, _i_reg_17_, _size_reg_14_
	bgez	$6, _loop_end_19_
# was:	bgez	_tmp_reg_20_, _loop_end_19_
	sw	$5, 0($4)
# was:	sw	_i_reg_17_, 0(_addr_reg_16_)
	addi	$4, $4, 4
# was:	addi	_addr_reg_16_, _addr_reg_16_, 4
	addi	$5, $5, 1
# was:	addi	_i_reg_17_, _i_reg_17_, 1
	j	_loop_beg_18_
_loop_end_19_:
	ori	$2, $3, 0
# was:	ori	_arr_reg_23_, _letBind_13_, 0
	lw	$16, 0($2)
# was:	lw	_size_reg_22_, 0(_arr_reg_23_)
	ori	$17, $28, 0
# was:	ori	_readIntArrres_12_, 28, 0
	sll	$3, $16, 2
# was:	sll	_tmp_32_, _size_reg_22_, 2
	addi	$3, $3, 4
# was:	addi	_tmp_32_, _tmp_32_, 4
	add	$28, $28, $3
# was:	add	28, 28, _tmp_32_
	sw	$16, 0($17)
# was:	sw	_size_reg_22_, 0(_readIntArrres_12_)
	addi	$18, $17, 4
# was:	addi	_addr_reg_26_, _readIntArrres_12_, 4
	ori	$19, $0, 0
# was:	ori	_i_reg_27_, 0, 0
	addi	$20, $2, 4
# was:	addi	_elem_reg_24_, _arr_reg_23_, 4
_loop_beg_28_:
	sub	$2, $19, $16
# was:	sub	_tmp_reg_30_, _i_reg_27_, _size_reg_22_
	bgez	$2, _loop_end_29_
# was:	bgez	_tmp_reg_30_, _loop_end_29_
	lw	$2, 0($20)
# was:	lw	_res_reg_25_, 0(_elem_reg_24_)
# 	ori	2,_res_reg_25_,0
	jal	readInt
# was:	jal	readInt, 2
# 	ori	_tmp_reg_31_,2,0
# 	ori	_res_reg_25_,_tmp_reg_31_,0
	addi	$20, $20, 4
# was:	addi	_elem_reg_24_, _elem_reg_24_, 4
	sw	$2, 0($18)
# was:	sw	_res_reg_25_, 0(_addr_reg_26_)
	addi	$18, $18, 4
# was:	addi	_addr_reg_26_, _addr_reg_26_, 4
	addi	$19, $19, 1
# was:	addi	_i_reg_27_, _i_reg_27_, 1
	j	_loop_beg_28_
_loop_end_29_:
	ori	$2, $17, 0
# was:	ori	2, _readIntArrres_12_, 0
	addi	$29, $29, 28
	lw	$20, -24($29)
	lw	$19, -20($29)
	lw	$18, -16($29)
	lw	$17, -12($29)
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function writeInt
writeInt:
	sw	$31, -4($29)
	sw	$16, -8($29)
	addi	$29, $29, -12
# 	ori	_param_i_33_,2,0
	ori	$16, $2, 0
# was:	ori	_tmp_35_, _param_i_33_, 0
# 	ori	_writeIntres_34_,_tmp_35_,0
	ori	$2, $16, 0
# was:	ori	2, _writeIntres_34_, 0
	jal	putint
# was:	jal	putint, 2
	ori	$2, $16, 0
# was:	ori	2, _writeIntres_34_, 0
	addi	$29, $29, 12
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function writeIntArr
writeIntArr:
	sw	$31, -4($29)
	sw	$19, -20($29)
	sw	$18, -16($29)
	sw	$17, -12($29)
	sw	$16, -8($29)
	addi	$29, $29, -24
	ori	$16, $2, 0
# was:	ori	_param_arr_36_, 2, 0
	la	$2, ___str___40_
# was:	la	_tmp_39_, ___str___40_
# ___str___40_: string " { "
# 	ori	_letBind_38_,_tmp_39_,0
# 	ori	2,_tmp_39_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	_arr_reg_43_, _param_arr_36_, 0
	lw	$16, 0($2)
# was:	lw	_size_reg_42_, 0(_arr_reg_43_)
	ori	$4, $28, 0
# was:	ori	_letBind_41_, 28, 0
	sll	$3, $16, 2
# was:	sll	_tmp_52_, _size_reg_42_, 2
	addi	$3, $3, 4
# was:	addi	_tmp_52_, _tmp_52_, 4
	add	$28, $28, $3
# was:	add	28, 28, _tmp_52_
	sw	$16, 0($4)
# was:	sw	_size_reg_42_, 0(_letBind_41_)
	addi	$18, $4, 4
# was:	addi	_addr_reg_46_, _letBind_41_, 4
	ori	$17, $0, 0
# was:	ori	_i_reg_47_, 0, 0
	addi	$19, $2, 4
# was:	addi	_elem_reg_44_, _arr_reg_43_, 4
_loop_beg_48_:
	sub	$2, $17, $16
# was:	sub	_tmp_reg_50_, _i_reg_47_, _size_reg_42_
	bgez	$2, _loop_end_49_
# was:	bgez	_tmp_reg_50_, _loop_end_49_
	lw	$2, 0($19)
# was:	lw	_res_reg_45_, 0(_elem_reg_44_)
# 	ori	2,_res_reg_45_,0
	jal	writeInt
# was:	jal	writeInt, 2
# 	ori	_tmp_reg_51_,2,0
# 	ori	_res_reg_45_,_tmp_reg_51_,0
	addi	$19, $19, 4
# was:	addi	_elem_reg_44_, _elem_reg_44_, 4
	sw	$2, 0($18)
# was:	sw	_res_reg_45_, 0(_addr_reg_46_)
	addi	$18, $18, 4
# was:	addi	_addr_reg_46_, _addr_reg_46_, 4
	addi	$17, $17, 1
# was:	addi	_i_reg_47_, _i_reg_47_, 1
	j	_loop_beg_48_
_loop_end_49_:
	la	$2, ___str___55_
# was:	la	_tmp_54_, ___str___55_
# ___str___55_: string " }\n"
# 	ori	_letBind_53_,_tmp_54_,0
# 	ori	2,_tmp_54_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $0, 1
# was:	ori	_writeIntArrres_37_, 0, 1
# 	ori	2,_writeIntArrres_37_,0
	addi	$29, $29, 24
	lw	$19, -20($29)
	lw	$18, -16($29)
	lw	$17, -12($29)
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function max
max:
	sw	$31, -4($29)
	addi	$29, $29, -8
# 	ori	_param_x_56_,2,0
# 	ori	_param_y_57_,3,0
# 	ori	_lt_L_63_,_param_x_56_,0
# 	ori	_lt_R_64_,_param_y_57_,0
	slt	$4, $2, $3
# was:	slt	_cond_62_, _lt_L_63_, _lt_R_64_
	bne	$4, $0, _then_59_
# was:	bne	_cond_62_, 0, _then_59_
	j	_else_60_
_then_59_:
	ori	$2, $3, 0
# was:	ori	_maxres_58_, _param_y_57_, 0
	j	_endif_61_
_else_60_:
# 	ori	_maxres_58_,_param_x_56_,0
_endif_61_:
# 	ori	2,_maxres_58_,0
	addi	$29, $29, 8
	lw	$31, -4($29)
	jr	$31
# Function mapfun
mapfun:
	sw	$31, -4($29)
	sw	$16, -8($29)
	addi	$29, $29, -12
	ori	$16, $2, 0
# was:	ori	_param_x_65_, 2, 0
	ori	$2, $16, 0
# was:	ori	_arg_68_, _param_x_65_, 0
	ori	$3, $0, 0
# was:	ori	_arg_69_, 0, 0
# 	ori	2,_arg_68_,0
# 	ori	3,_arg_69_,0
	jal	max
# was:	jal	max, 23
# 	ori	_letBind_67_,2,0
	ori	$5, $0, 4
# was:	ori	_size_reg_70_, 0, 4
	ori	$3, $28, 0
# was:	ori	_mapfunres_66_, 28, 0
	sll	$4, $5, 2
# was:	sll	_tmp_73_, _size_reg_70_, 2
	addi	$4, $4, 4
# was:	addi	_tmp_73_, _tmp_73_, 4
	add	$28, $28, $4
# was:	add	28, 28, _tmp_73_
	sw	$5, 0($3)
# was:	sw	_size_reg_70_, 0(_mapfunres_66_)
	addi	$4, $3, 4
# was:	addi	_addr_reg_71_, _mapfunres_66_, 4
# 	ori	_tmp_reg_72_,_letBind_67_,0
	sw	$2, 0($4)
# was:	sw	_tmp_reg_72_, 0(_addr_reg_71_)
	addi	$4, $4, 4
# was:	addi	_addr_reg_71_, _addr_reg_71_, 4
# 	ori	_tmp_reg_72_,_letBind_67_,0
	sw	$2, 0($4)
# was:	sw	_tmp_reg_72_, 0(_addr_reg_71_)
	addi	$4, $4, 4
# was:	addi	_addr_reg_71_, _addr_reg_71_, 4
# 	ori	_tmp_reg_72_,_letBind_67_,0
	sw	$2, 0($4)
# was:	sw	_tmp_reg_72_, 0(_addr_reg_71_)
	addi	$4, $4, 4
# was:	addi	_addr_reg_71_, _addr_reg_71_, 4
	ori	$2, $16, 0
# was:	ori	_tmp_reg_72_, _param_x_65_, 0
	sw	$2, 0($4)
# was:	sw	_tmp_reg_72_, 0(_addr_reg_71_)
	addi	$4, $4, 4
# was:	addi	_addr_reg_71_, _addr_reg_71_, 4
	ori	$2, $3, 0
# was:	ori	2, _mapfunres_66_, 0
	addi	$29, $29, 12
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function redfun
redfun:
	sw	$31, -4($29)
	sw	$19, -20($29)
	sw	$18, -16($29)
	sw	$17, -12($29)
	sw	$16, -8($29)
	addi	$29, $29, -24
	ori	$18, $2, 0
# was:	ori	_param_a_74_, 2, 0
	ori	$16, $3, 0
# was:	ori	_param_b_75_, 3, 0
	ori	$3, $0, 0
# was:	ori	_arr_ind_80_, 0, 0
	addi	$2, $18, 4
# was:	addi	_arr_reg_81_, _param_a_74_, 4
	lw	$4, 0($18)
# was:	lw	_size_reg_82_, 0(_param_a_74_)
	bgez	$3, _safe_lab_85_
# was:	bgez	_arr_ind_80_, _safe_lab_85_
_error_lab_84_:
	ori	$5, $0, 23
# was:	ori	5, 0, 23
	j	_IllegalArrSizeError_
_safe_lab_85_:
	sub	$4, $3, $4
# was:	sub	_tmp_reg_83_, _arr_ind_80_, _size_reg_82_
	bgez	$4, _error_lab_84_
# was:	bgez	_tmp_reg_83_, _error_lab_84_
	sll	$3, $3, 2
# was:	sll	_arr_ind_80_, _arr_ind_80_, 2
	add	$2, $2, $3
# was:	add	_arr_reg_81_, _arr_reg_81_, _arr_ind_80_
	lw	$3, 0($2)
# was:	lw	_arg_79_, 0(_arr_reg_81_)
	ori	$5, $0, 0
# was:	ori	_arr_ind_87_, 0, 0
	addi	$4, $16, 4
# was:	addi	_arr_reg_88_, _param_b_75_, 4
	lw	$2, 0($16)
# was:	lw	_size_reg_89_, 0(_param_b_75_)
	bgez	$5, _safe_lab_92_
# was:	bgez	_arr_ind_87_, _safe_lab_92_
_error_lab_91_:
	ori	$5, $0, 23
# was:	ori	5, 0, 23
	j	_IllegalArrSizeError_
_safe_lab_92_:
	sub	$2, $5, $2
# was:	sub	_tmp_reg_90_, _arr_ind_87_, _size_reg_89_
	bgez	$2, _error_lab_91_
# was:	bgez	_tmp_reg_90_, _error_lab_91_
	sll	$5, $5, 2
# was:	sll	_arr_ind_87_, _arr_ind_87_, 2
	add	$4, $4, $5
# was:	add	_arr_reg_88_, _arr_reg_88_, _arr_ind_87_
	lw	$4, 0($4)
# was:	lw	_arg_86_, 0(_arr_reg_88_)
	ori	$2, $3, 0
# was:	ori	2, _arg_79_, 0
	ori	$3, $4, 0
# was:	ori	3, _arg_86_, 0
	jal	max
# was:	jal	max, 23
# 	ori	_arg_78_,2,0
	ori	$4, $0, 2
# was:	ori	_arr_ind_96_, 0, 2
	addi	$5, $18, 4
# was:	addi	_arr_reg_97_, _param_a_74_, 4
	lw	$3, 0($18)
# was:	lw	_size_reg_98_, 0(_param_a_74_)
	bgez	$4, _safe_lab_101_
# was:	bgez	_arr_ind_96_, _safe_lab_101_
_error_lab_100_:
	ori	$5, $0, 23
# was:	ori	5, 0, 23
	j	_IllegalArrSizeError_
_safe_lab_101_:
	sub	$3, $4, $3
# was:	sub	_tmp_reg_99_, _arr_ind_96_, _size_reg_98_
	bgez	$3, _error_lab_100_
# was:	bgez	_tmp_reg_99_, _error_lab_100_
	sll	$4, $4, 2
# was:	sll	_arr_ind_96_, _arr_ind_96_, 2
	add	$5, $5, $4
# was:	add	_arr_reg_97_, _arr_reg_97_, _arr_ind_96_
	lw	$3, 0($5)
# was:	lw	_plus_L_94_, 0(_arr_reg_97_)
	ori	$4, $0, 1
# was:	ori	_arr_ind_102_, 0, 1
	addi	$5, $16, 4
# was:	addi	_arr_reg_103_, _param_b_75_, 4
	lw	$6, 0($16)
# was:	lw	_size_reg_104_, 0(_param_b_75_)
	bgez	$4, _safe_lab_107_
# was:	bgez	_arr_ind_102_, _safe_lab_107_
_error_lab_106_:
	ori	$5, $0, 23
# was:	ori	5, 0, 23
	j	_IllegalArrSizeError_
_safe_lab_107_:
	sub	$6, $4, $6
# was:	sub	_tmp_reg_105_, _arr_ind_102_, _size_reg_104_
	bgez	$6, _error_lab_106_
# was:	bgez	_tmp_reg_105_, _error_lab_106_
	sll	$4, $4, 2
# was:	sll	_arr_ind_102_, _arr_ind_102_, 2
	add	$5, $5, $4
# was:	add	_arr_reg_103_, _arr_reg_103_, _arr_ind_102_
	lw	$4, 0($5)
# was:	lw	_plus_R_95_, 0(_arr_reg_103_)
	add	$3, $3, $4
# was:	add	_arg_93_, _plus_L_94_, _plus_R_95_
# 	ori	2,_arg_78_,0
# 	ori	3,_arg_93_,0
	jal	max
# was:	jal	max, 23
	ori	$17, $2, 0
# was:	ori	_letBind_77_, 2, 0
	ori	$3, $0, 1
# was:	ori	_arr_ind_110_, 0, 1
	addi	$2, $18, 4
# was:	addi	_arr_reg_111_, _param_a_74_, 4
	lw	$4, 0($18)
# was:	lw	_size_reg_112_, 0(_param_a_74_)
	bgez	$3, _safe_lab_115_
# was:	bgez	_arr_ind_110_, _safe_lab_115_
_error_lab_114_:
	ori	$5, $0, 24
# was:	ori	5, 0, 24
	j	_IllegalArrSizeError_
_safe_lab_115_:
	sub	$4, $3, $4
# was:	sub	_tmp_reg_113_, _arr_ind_110_, _size_reg_112_
	bgez	$4, _error_lab_114_
# was:	bgez	_tmp_reg_113_, _error_lab_114_
	sll	$3, $3, 2
# was:	sll	_arr_ind_110_, _arr_ind_110_, 2
	add	$2, $2, $3
# was:	add	_arr_reg_111_, _arr_reg_111_, _arr_ind_110_
	lw	$2, 0($2)
# was:	lw	_arg_109_, 0(_arr_reg_111_)
	ori	$4, $0, 3
# was:	ori	_arr_ind_119_, 0, 3
	addi	$3, $18, 4
# was:	addi	_arr_reg_120_, _param_a_74_, 4
	lw	$5, 0($18)
# was:	lw	_size_reg_121_, 0(_param_a_74_)
	bgez	$4, _safe_lab_124_
# was:	bgez	_arr_ind_119_, _safe_lab_124_
_error_lab_123_:
	ori	$5, $0, 24
# was:	ori	5, 0, 24
	j	_IllegalArrSizeError_
_safe_lab_124_:
	sub	$5, $4, $5
# was:	sub	_tmp_reg_122_, _arr_ind_119_, _size_reg_121_
	bgez	$5, _error_lab_123_
# was:	bgez	_tmp_reg_122_, _error_lab_123_
	sll	$4, $4, 2
# was:	sll	_arr_ind_119_, _arr_ind_119_, 2
	add	$3, $3, $4
# was:	add	_arr_reg_120_, _arr_reg_120_, _arr_ind_119_
	lw	$3, 0($3)
# was:	lw	_plus_L_117_, 0(_arr_reg_120_)
	ori	$4, $0, 1
# was:	ori	_arr_ind_125_, 0, 1
	addi	$5, $16, 4
# was:	addi	_arr_reg_126_, _param_b_75_, 4
	lw	$6, 0($16)
# was:	lw	_size_reg_127_, 0(_param_b_75_)
	bgez	$4, _safe_lab_130_
# was:	bgez	_arr_ind_125_, _safe_lab_130_
_error_lab_129_:
	ori	$5, $0, 24
# was:	ori	5, 0, 24
	j	_IllegalArrSizeError_
_safe_lab_130_:
	sub	$6, $4, $6
# was:	sub	_tmp_reg_128_, _arr_ind_125_, _size_reg_127_
	bgez	$6, _error_lab_129_
# was:	bgez	_tmp_reg_128_, _error_lab_129_
	sll	$4, $4, 2
# was:	sll	_arr_ind_125_, _arr_ind_125_, 2
	add	$5, $5, $4
# was:	add	_arr_reg_126_, _arr_reg_126_, _arr_ind_125_
	lw	$4, 0($5)
# was:	lw	_plus_R_118_, 0(_arr_reg_126_)
	add	$3, $3, $4
# was:	add	_arg_116_, _plus_L_117_, _plus_R_118_
# 	ori	2,_arg_109_,0
# 	ori	3,_arg_116_,0
	jal	max
# was:	jal	max, 23
	ori	$19, $2, 0
# was:	ori	_letBind_108_, 2, 0
	ori	$3, $0, 2
# was:	ori	_arr_ind_135_, 0, 2
	addi	$2, $18, 4
# was:	addi	_arr_reg_136_, _param_a_74_, 4
	lw	$4, 0($18)
# was:	lw	_size_reg_137_, 0(_param_a_74_)
	bgez	$3, _safe_lab_140_
# was:	bgez	_arr_ind_135_, _safe_lab_140_
_error_lab_139_:
	ori	$5, $0, 25
# was:	ori	5, 0, 25
	j	_IllegalArrSizeError_
_safe_lab_140_:
	sub	$4, $3, $4
# was:	sub	_tmp_reg_138_, _arr_ind_135_, _size_reg_137_
	bgez	$4, _error_lab_139_
# was:	bgez	_tmp_reg_138_, _error_lab_139_
	sll	$3, $3, 2
# was:	sll	_arr_ind_135_, _arr_ind_135_, 2
	add	$2, $2, $3
# was:	add	_arr_reg_136_, _arr_reg_136_, _arr_ind_135_
	lw	$3, 0($2)
# was:	lw	_plus_L_133_, 0(_arr_reg_136_)
	ori	$4, $0, 3
# was:	ori	_arr_ind_141_, 0, 3
	addi	$2, $16, 4
# was:	addi	_arr_reg_142_, _param_b_75_, 4
	lw	$5, 0($16)
# was:	lw	_size_reg_143_, 0(_param_b_75_)
	bgez	$4, _safe_lab_146_
# was:	bgez	_arr_ind_141_, _safe_lab_146_
_error_lab_145_:
	ori	$5, $0, 25
# was:	ori	5, 0, 25
	j	_IllegalArrSizeError_
_safe_lab_146_:
	sub	$5, $4, $5
# was:	sub	_tmp_reg_144_, _arr_ind_141_, _size_reg_143_
	bgez	$5, _error_lab_145_
# was:	bgez	_tmp_reg_144_, _error_lab_145_
	sll	$4, $4, 2
# was:	sll	_arr_ind_141_, _arr_ind_141_, 2
	add	$2, $2, $4
# was:	add	_arr_reg_142_, _arr_reg_142_, _arr_ind_141_
	lw	$2, 0($2)
# was:	lw	_plus_R_134_, 0(_arr_reg_142_)
	add	$2, $3, $2
# was:	add	_arg_132_, _plus_L_133_, _plus_R_134_
	ori	$4, $0, 2
# was:	ori	_arr_ind_148_, 0, 2
	addi	$3, $16, 4
# was:	addi	_arr_reg_149_, _param_b_75_, 4
	lw	$5, 0($16)
# was:	lw	_size_reg_150_, 0(_param_b_75_)
	bgez	$4, _safe_lab_153_
# was:	bgez	_arr_ind_148_, _safe_lab_153_
_error_lab_152_:
	ori	$5, $0, 25
# was:	ori	5, 0, 25
	j	_IllegalArrSizeError_
_safe_lab_153_:
	sub	$5, $4, $5
# was:	sub	_tmp_reg_151_, _arr_ind_148_, _size_reg_150_
	bgez	$5, _error_lab_152_
# was:	bgez	_tmp_reg_151_, _error_lab_152_
	sll	$4, $4, 2
# was:	sll	_arr_ind_148_, _arr_ind_148_, 2
	add	$3, $3, $4
# was:	add	_arr_reg_149_, _arr_reg_149_, _arr_ind_148_
	lw	$3, 0($3)
# was:	lw	_arg_147_, 0(_arr_reg_149_)
# 	ori	2,_arg_132_,0
# 	ori	3,_arg_147_,0
	jal	max
# was:	jal	max, 23
# 	ori	_letBind_131_,2,0
	ori	$3, $0, 3
# was:	ori	_arr_ind_157_, 0, 3
	addi	$4, $18, 4
# was:	addi	_arr_reg_158_, _param_a_74_, 4
	lw	$5, 0($18)
# was:	lw	_size_reg_159_, 0(_param_a_74_)
	bgez	$3, _safe_lab_162_
# was:	bgez	_arr_ind_157_, _safe_lab_162_
_error_lab_161_:
	ori	$5, $0, 26
# was:	ori	5, 0, 26
	j	_IllegalArrSizeError_
_safe_lab_162_:
	sub	$5, $3, $5
# was:	sub	_tmp_reg_160_, _arr_ind_157_, _size_reg_159_
	bgez	$5, _error_lab_161_
# was:	bgez	_tmp_reg_160_, _error_lab_161_
	sll	$3, $3, 2
# was:	sll	_arr_ind_157_, _arr_ind_157_, 2
	add	$4, $4, $3
# was:	add	_arr_reg_158_, _arr_reg_158_, _arr_ind_157_
	lw	$5, 0($4)
# was:	lw	_plus_L_155_, 0(_arr_reg_158_)
	ori	$3, $0, 3
# was:	ori	_arr_ind_163_, 0, 3
	addi	$4, $16, 4
# was:	addi	_arr_reg_164_, _param_b_75_, 4
	lw	$6, 0($16)
# was:	lw	_size_reg_165_, 0(_param_b_75_)
	bgez	$3, _safe_lab_168_
# was:	bgez	_arr_ind_163_, _safe_lab_168_
_error_lab_167_:
	ori	$5, $0, 26
# was:	ori	5, 0, 26
	j	_IllegalArrSizeError_
_safe_lab_168_:
	sub	$6, $3, $6
# was:	sub	_tmp_reg_166_, _arr_ind_163_, _size_reg_165_
	bgez	$6, _error_lab_167_
# was:	bgez	_tmp_reg_166_, _error_lab_167_
	sll	$3, $3, 2
# was:	sll	_arr_ind_163_, _arr_ind_163_, 2
	add	$4, $4, $3
# was:	add	_arr_reg_164_, _arr_reg_164_, _arr_ind_163_
	lw	$3, 0($4)
# was:	lw	_plus_R_156_, 0(_arr_reg_164_)
	add	$3, $5, $3
# was:	add	_letBind_154_, _plus_L_155_, _plus_R_156_
	ori	$6, $0, 4
# was:	ori	_size_reg_169_, 0, 4
	ori	$4, $28, 0
# was:	ori	_redfunres_76_, 28, 0
	sll	$5, $6, 2
# was:	sll	_tmp_172_, _size_reg_169_, 2
	addi	$5, $5, 4
# was:	addi	_tmp_172_, _tmp_172_, 4
	add	$28, $28, $5
# was:	add	28, 28, _tmp_172_
	sw	$6, 0($4)
# was:	sw	_size_reg_169_, 0(_redfunres_76_)
	addi	$5, $4, 4
# was:	addi	_addr_reg_170_, _redfunres_76_, 4
	ori	$6, $17, 0
# was:	ori	_tmp_reg_171_, _letBind_77_, 0
	sw	$6, 0($5)
# was:	sw	_tmp_reg_171_, 0(_addr_reg_170_)
	addi	$5, $5, 4
# was:	addi	_addr_reg_170_, _addr_reg_170_, 4
	ori	$6, $19, 0
# was:	ori	_tmp_reg_171_, _letBind_108_, 0
	sw	$6, 0($5)
# was:	sw	_tmp_reg_171_, 0(_addr_reg_170_)
	addi	$5, $5, 4
# was:	addi	_addr_reg_170_, _addr_reg_170_, 4
	ori	$6, $2, 0
# was:	ori	_tmp_reg_171_, _letBind_131_, 0
	sw	$6, 0($5)
# was:	sw	_tmp_reg_171_, 0(_addr_reg_170_)
	addi	$5, $5, 4
# was:	addi	_addr_reg_170_, _addr_reg_170_, 4
	ori	$6, $3, 0
# was:	ori	_tmp_reg_171_, _letBind_154_, 0
	sw	$6, 0($5)
# was:	sw	_tmp_reg_171_, 0(_addr_reg_170_)
	addi	$5, $5, 4
# was:	addi	_addr_reg_170_, _addr_reg_170_, 4
	ori	$2, $4, 0
# was:	ori	2, _redfunres_76_, 0
	addi	$29, $29, 24
	lw	$19, -20($29)
	lw	$18, -16($29)
	lw	$17, -12($29)
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function mssp
mssp:
	sw	$31, -4($29)
	sw	$20, -24($29)
	sw	$19, -20($29)
	sw	$18, -16($29)
	sw	$17, -12($29)
	sw	$16, -8($29)
	addi	$29, $29, -28
# 	ori	_param_n_173_,2,0
# 	ori	_arg_176_,_param_n_173_,0
# 	ori	2,_arg_176_,0
	jal	readIntArr
# was:	jal	readIntArr, 2
# 	ori	_letBind_175_,2,0
# 	ori	_arr_reg_179_,_letBind_175_,0
	lw	$17, 0($2)
# was:	lw	_size_reg_178_, 0(_arr_reg_179_)
	ori	$16, $28, 0
# was:	ori	_letBind_177_, 28, 0
	sll	$3, $17, 2
# was:	sll	_tmp_188_, _size_reg_178_, 2
	addi	$3, $3, 4
# was:	addi	_tmp_188_, _tmp_188_, 4
	add	$28, $28, $3
# was:	add	28, 28, _tmp_188_
	sw	$17, 0($16)
# was:	sw	_size_reg_178_, 0(_letBind_177_)
	addi	$18, $16, 4
# was:	addi	_addr_reg_182_, _letBind_177_, 4
	ori	$19, $0, 0
# was:	ori	_i_reg_183_, 0, 0
	addi	$20, $2, 4
# was:	addi	_elem_reg_180_, _arr_reg_179_, 4
_loop_beg_184_:
	sub	$2, $19, $17
# was:	sub	_tmp_reg_186_, _i_reg_183_, _size_reg_178_
	bgez	$2, _loop_end_185_
# was:	bgez	_tmp_reg_186_, _loop_end_185_
	lw	$2, 0($20)
# was:	lw	_res_reg_181_, 0(_elem_reg_180_)
# 	ori	2,_res_reg_181_,0
	jal	mapfun
# was:	jal	mapfun, 2
# 	ori	_tmp_reg_187_,2,0
# 	ori	_res_reg_181_,_tmp_reg_187_,0
	addi	$20, $20, 4
# was:	addi	_elem_reg_180_, _elem_reg_180_, 4
	sw	$2, 0($18)
# was:	sw	_res_reg_181_, 0(_addr_reg_182_)
	addi	$18, $18, 4
# was:	addi	_addr_reg_182_, _addr_reg_182_, 4
	addi	$19, $19, 1
# was:	addi	_i_reg_183_, _i_reg_183_, 1
	j	_loop_beg_184_
_loop_end_185_:
	ori	$4, $0, 4
# was:	ori	_size_reg_190_, 0, 4
	ori	$2, $28, 0
# was:	ori	_letBind_189_, 28, 0
	sll	$3, $4, 2
# was:	sll	_tmp_193_, _size_reg_190_, 2
	addi	$3, $3, 4
# was:	addi	_tmp_193_, _tmp_193_, 4
	add	$28, $28, $3
# was:	add	28, 28, _tmp_193_
	sw	$4, 0($2)
# was:	sw	_size_reg_190_, 0(_letBind_189_)
	addi	$3, $2, 4
# was:	addi	_addr_reg_191_, _letBind_189_, 4
	ori	$4, $0, 0
# was:	ori	_tmp_reg_192_, 0, 0
	sw	$4, 0($3)
# was:	sw	_tmp_reg_192_, 0(_addr_reg_191_)
	addi	$3, $3, 4
# was:	addi	_addr_reg_191_, _addr_reg_191_, 4
	ori	$4, $0, 0
# was:	ori	_tmp_reg_192_, 0, 0
	sw	$4, 0($3)
# was:	sw	_tmp_reg_192_, 0(_addr_reg_191_)
	addi	$3, $3, 4
# was:	addi	_addr_reg_191_, _addr_reg_191_, 4
	ori	$4, $0, 0
# was:	ori	_tmp_reg_192_, 0, 0
	sw	$4, 0($3)
# was:	sw	_tmp_reg_192_, 0(_addr_reg_191_)
	addi	$3, $3, 4
# was:	addi	_addr_reg_191_, _addr_reg_191_, 4
	ori	$4, $0, 0
# was:	ori	_tmp_reg_192_, 0, 0
	sw	$4, 0($3)
# was:	sw	_tmp_reg_192_, 0(_addr_reg_191_)
	addi	$3, $3, 4
# was:	addi	_addr_reg_191_, _addr_reg_191_, 4
# 	ori	_arr_reg_194_,_letBind_177_,0
	lw	$17, 0($16)
# was:	lw	_size_reg_195_, 0(_arr_reg_194_)
# 	ori	_msspres_174_,_letBind_189_,0
	addi	$16, $16, 4
# was:	addi	_arr_reg_194_, _arr_reg_194_, 4
	ori	$18, $0, 0
# was:	ori	_ind_var_196_, 0, 0
_loop_beg_198_:
	sub	$3, $18, $17
# was:	sub	_tmp_reg_197_, _ind_var_196_, _size_reg_195_
	bgez	$3, _loop_end_199_
# was:	bgez	_tmp_reg_197_, _loop_end_199_
	lw	$3, 0($16)
# was:	lw	_tmp_reg_197_, 0(_arr_reg_194_)
	addi	$16, $16, 4
# was:	addi	_arr_reg_194_, _arr_reg_194_, 4
# 	ori	2,_msspres_174_,0
# 	ori	3,_tmp_reg_197_,0
	jal	redfun
# was:	jal	redfun, 23
# 	ori	_tmp_reg_200_,2,0
# 	ori	_msspres_174_,_tmp_reg_200_,0
	addi	$18, $18, 1
# was:	addi	_ind_var_196_, _ind_var_196_, 1
	j	_loop_beg_198_
_loop_end_199_:
# 	ori	2,_msspres_174_,0
	addi	$29, $29, 28
	lw	$20, -24($29)
	lw	$19, -20($29)
	lw	$18, -16($29)
	lw	$17, -12($29)
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function main
main:
	sw	$31, -4($29)
	sw	$16, -8($29)
	addi	$29, $29, -12
	ori	$2, $0, 8
# was:	ori	_arg_203_, 0, 8
# 	ori	2,_arg_203_,0
	jal	mssp
# was:	jal	mssp, 2
	ori	$16, $2, 0
# was:	ori	_letBind_202_, 2, 0
	la	$2, _MSSPRes_206_
# was:	la	_tmp_205_, _MSSPRes_206_
# _MSSPRes_206_: string "\n\nMSSP Result Is: "
# 	ori	_letBind_204_,_tmp_205_,0
# 	ori	2,_tmp_205_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$3, $0, 0
# was:	ori	_arr_ind_209_, 0, 0
	addi	$2, $16, 4
# was:	addi	_arr_reg_210_, _letBind_202_, 4
	lw	$4, 0($16)
# was:	lw	_size_reg_211_, 0(_letBind_202_)
	bgez	$3, _safe_lab_214_
# was:	bgez	_arr_ind_209_, _safe_lab_214_
_error_lab_213_:
	ori	$5, $0, 40
# was:	ori	5, 0, 40
	j	_IllegalArrSizeError_
_safe_lab_214_:
	sub	$4, $3, $4
# was:	sub	_tmp_reg_212_, _arr_ind_209_, _size_reg_211_
	bgez	$4, _error_lab_213_
# was:	bgez	_tmp_reg_212_, _error_lab_213_
	sll	$3, $3, 2
# was:	sll	_arr_ind_209_, _arr_ind_209_, 2
	add	$2, $2, $3
# was:	add	_arr_reg_210_, _arr_reg_210_, _arr_ind_209_
	lw	$2, 0($2)
# was:	lw	_arg_208_, 0(_arr_reg_210_)
# 	ori	2,_arg_208_,0
	jal	writeInt
# was:	jal	writeInt, 2
	ori	$16, $2, 0
# was:	ori	_letBind_207_, 2, 0
	la	$2, ___str___217_
# was:	la	_tmp_216_, ___str___217_
# ___str___217_: string "\n"
# 	ori	_letBind_215_,_tmp_216_,0
# 	ori	2,_tmp_216_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	_mainres_201_, _letBind_207_, 0
# 	ori	2,_mainres_201_,0
	addi	$29, $29, 12
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
ord:
	jr	$31
chr:
	andi	$2, $2, 255
	jr	$31
putint:
	addi	$29, $29, -8
	sw	$2, 0($29)
	sw	$4, 4($29)
	ori	$4, $2, 0
	ori	$2, $0, 1
	syscall
	ori	$2, $0, 4
	la	$4, _space_
	syscall
	lw	$2, 0($29)
	lw	$4, 4($29)
	addi	$29, $29, 8
	jr	$31
getint:
	ori	$2, $0, 5
	syscall
	jr	$31
putchar:
	addi	$29, $29, -8
	sw	$2, 0($29)
	sw	$4, 4($29)
	ori	$4, $2, 0
	ori	$2, $0, 11
	syscall
	ori	$2, $0, 4
	la	$4, _space_
	syscall
	lw	$2, 0($29)
	lw	$4, 4($29)
	addi	$29, $29, 8
	jr	$31
dynalloc:
	ori	$4, $2, 0
	ori	$2, $0, 9
	syscall
	jr	$31
getchar:
	addi	$29, $29, -8
	sw	$4, 0($29)
	sw	$5, 4($29)
	ori	$2, $0, 12
	syscall
	ori	$5, $2, 0
	ori	$2, $0, 4
	la	$4, _cr_
	syscall
	ori	$2, $5, 0
	lw	$4, 0($29)
	lw	$5, 4($29)
	addi	$29, $29, 8
	jr	$31
putstring:
	addi	$29, $29, -16
	sw	$2, 0($29)
	sw	$4, 4($29)
	sw	$5, 8($29)
	sw	$6, 12($29)
	lw	$4, 0($2)
	addi	$5, $2, 4
	add	$6, $5, $4
	ori	$2, $0, 11
putstring_begin:
	sub	$4, $5, $6
	bgez	$4, putstring_done
	lb	$4, 0($5)
	syscall
	addi	$5, $5, 1
	j	putstring_begin
putstring_done:
	lw	$2, 0($29)
	lw	$4, 4($29)
	lw	$5, 8($29)
	lw	$6, 12($29)
	addi	$29, $29, 16
	jr	$31
_IllegalArrSizeError_:
	la	$4, _IllegalArrSizeString_
	ori	$2, $0, 4
	syscall
	ori	$4, $5, 0
	ori	$2, $0, 1
	syscall
	la	$4, _cr_
	ori	$2, $0, 4
	syscall
	j	_stop_
	.data	
_cr_:
	.asciiz	"\n"
_space_:
	.asciiz	" "
_IllegalArrSizeString_:
	.asciiz	"Error: Array size less or equal to 0 at line "
# String Literals
	.align	2
___str___217_:
	.space	4
	.asciiz	"\n"
	.align	2
_MSSPRes_206_:
	.space	4
	.asciiz	"\n\nMSSP Result Is: "
	.align	2
___str___55_:
	.space	4
	.asciiz	" }\n"
	.align	2
___str___40_:
	.space	4
	.asciiz	" { "
	.align	2
___str___10_:
	.space	4
	.asciiz	": "
	.align	2
_Introdu_5_:
	.space	4
	.asciiz	"Introduce (Next) Number "
	.align	2
_true:
	.space	4
	.asciiz	"True"
	.align	2
_false:
	.space	4
	.asciiz	"False"
	.align	2
_heap_:
	.space	100000
