	.text	0x00400000
	.globl	main
	la	$28, _heap_
	la	$4, _true
# was:	la	_true_addr, _true
	ori	$3, $0, 4
# was:	ori	_true_init, 0, 4
	sw	$3, 0($4)
# was:	sw	_true_init, 0(_true_addr)
	la	$3, _false
# was:	la	_false_addr, _false
	ori	$4, $0, 5
# was:	ori	_false_init, 0, 5
	sw	$4, 0($3)
# was:	sw	_false_init, 0(_false_addr)
	jal	main
_stop_:
	ori	$2, $0, 10
	syscall
# Function main
main:
	sw	$31, -4($29)
	sw	$16, -8($29)
	addi	$29, $29, -12
	ori	$3, $0, 44
# was:	ori	_size_reg_3_, 0, 44
	addi	$3, $3, -1
# was:	addi	_size_reg_3_, _size_reg_3_, -1
	bgez	$3, _safe_lab_4_
# was:	bgez	_size_reg_3_, _safe_lab_4_
	ori	$5, $0, 2
# was:	ori	5, 0, 2
	j	_IllegalArrSizeError_
_safe_lab_4_:
	addi	$3, $3, 1
# was:	addi	_size_reg_3_, _size_reg_3_, 1
	ori	$4, $0, 117
# was:	ori	_elem_reg_5_, 0, 117
	ori	$2, $28, 0
# was:	ori	_letBind_2_, 28, 0
	addi	$5, $3, 3
# was:	addi	_tmp_11_, _size_reg_3_, 3
	sra	$5, $5, 2
# was:	sra	_tmp_11_, _tmp_11_, 2
	sll	$5, $5, 2
# was:	sll	_tmp_11_, _tmp_11_, 2
	addi	$5, $5, 4
# was:	addi	_tmp_11_, _tmp_11_, 4
	add	$28, $28, $5
# was:	add	28, 28, _tmp_11_
	sw	$3, 0($2)
# was:	sw	_size_reg_3_, 0(_letBind_2_)
	addi	$5, $2, 4
# was:	addi	_addr_reg_6_, _letBind_2_, 4
	ori	$6, $0, 0
# was:	ori	_i_reg_7_, 0, 0
_loop_beg_8_:
	sub	$7, $6, $3
# was:	sub	_tmp_reg_10_, _i_reg_7_, _size_reg_3_
	bgez	$7, _loop_end_9_
# was:	bgez	_tmp_reg_10_, _loop_end_9_
	sb	$4, 0($5)
# was:	sb	_elem_reg_5_, 0(_addr_reg_6_)
	addi	$5, $5, 1
# was:	addi	_addr_reg_6_, _addr_reg_6_, 1
	addi	$6, $6, 1
# was:	addi	_i_reg_7_, _i_reg_7_, 1
	j	_loop_beg_8_
_loop_end_9_:
# 	ori	_tmp_12_,_letBind_2_,0
	ori	$16, $2, 0
# was:	ori	_mainres_1_, _tmp_12_, 0
# 	ori	2,_tmp_12_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	2, _mainres_1_, 0
	addi	$29, $29, 12
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
ord:
	jr	$31
chr:
	andi	$2, $2, 255
	jr	$31
putint:
	addi	$29, $29, -8
	sw	$2, 0($29)
	sw	$4, 4($29)
	ori	$4, $2, 0
	ori	$2, $0, 1
	syscall
	ori	$2, $0, 4
	la	$4, _space_
	syscall
	lw	$2, 0($29)
	lw	$4, 4($29)
	addi	$29, $29, 8
	jr	$31
getint:
	ori	$2, $0, 5
	syscall
	jr	$31
putchar:
	addi	$29, $29, -8
	sw	$2, 0($29)
	sw	$4, 4($29)
	ori	$4, $2, 0
	ori	$2, $0, 11
	syscall
	ori	$2, $0, 4
	la	$4, _space_
	syscall
	lw	$2, 0($29)
	lw	$4, 4($29)
	addi	$29, $29, 8
	jr	$31
dynalloc:
	ori	$4, $2, 0
	ori	$2, $0, 9
	syscall
	jr	$31
getchar:
	addi	$29, $29, -8
	sw	$4, 0($29)
	sw	$5, 4($29)
	ori	$2, $0, 12
	syscall
	ori	$5, $2, 0
	ori	$2, $0, 4
	la	$4, _cr_
	syscall
	ori	$2, $5, 0
	lw	$4, 0($29)
	lw	$5, 4($29)
	addi	$29, $29, 8
	jr	$31
putstring:
	addi	$29, $29, -16
	sw	$2, 0($29)
	sw	$4, 4($29)
	sw	$5, 8($29)
	sw	$6, 12($29)
	lw	$4, 0($2)
	addi	$5, $2, 4
	add	$6, $5, $4
	ori	$2, $0, 11
putstring_begin:
	sub	$4, $5, $6
	bgez	$4, putstring_done
	lb	$4, 0($5)
	syscall
	addi	$5, $5, 1
	j	putstring_begin
putstring_done:
	lw	$2, 0($29)
	lw	$4, 4($29)
	lw	$5, 8($29)
	lw	$6, 12($29)
	addi	$29, $29, 16
	jr	$31
_IllegalArrSizeError_:
	la	$4, _IllegalArrSizeString_
	ori	$2, $0, 4
	syscall
	ori	$4, $5, 0
	ori	$2, $0, 1
	syscall
	la	$4, _cr_
	ori	$2, $0, 4
	syscall
	j	_stop_
	.data	
_cr_:
	.asciiz	"\n"
_space_:
	.asciiz	" "
_IllegalArrSizeString_:
	.asciiz	"Error: Array size less or equal to 0 at line "
# String Literals
	.align	2
_true:
	.space	4
	.asciiz	"True"
	.align	2
_false:
	.space	4
	.asciiz	"False"
	.align	2
_heap_:
	.space	100000
