	.text	0x00400000
	.globl	main
	la	$28, _heap_
	la	$4, ___str___25_
# was:	la	___str___25__addr, ___str___25_
	ori	$3, $0, 1
# was:	ori	___str___25__init, 0, 1
	sw	$3, 0($4)
# was:	sw	___str___25__init, 0(___str___25__addr)
	la	$4, ___str___11_
# was:	la	___str___11__addr, ___str___11_
	ori	$3, $0, 2
# was:	ori	___str___11__init, 0, 2
	sw	$3, 0($4)
# was:	sw	___str___11__init, 0(___str___11__addr)
	la	$4, ___str___6_
# was:	la	___str___6__addr, ___str___6_
	ori	$3, $0, 1
# was:	ori	___str___6__init, 0, 1
	sw	$3, 0($4)
# was:	sw	___str___6__init, 0(___str___6__addr)
	la	$4, _true
# was:	la	_true_addr, _true
	ori	$3, $0, 4
# was:	ori	_true_init, 0, 4
	sw	$3, 0($4)
# was:	sw	_true_init, 0(_true_addr)
	la	$3, _false
# was:	la	_false_addr, _false
	ori	$4, $0, 5
# was:	ori	_false_init, 0, 5
	sw	$4, 0($3)
# was:	sw	_false_init, 0(_false_addr)
	jal	main
_stop_:
	ori	$2, $0, 10
	syscall
# Function writeArrayNumber
writeArrayNumber:
	sw	$31, -4($29)
	sw	$16, -8($29)
	addi	$29, $29, -12
# 	ori	_param_n_1_,2,0
# 	ori	_tmp_4_,_param_n_1_,0
# 	ori	_letBind_3_,_tmp_4_,0
# 	ori	2,_letBind_3_,0
	jal	putint
# was:	jal	putint, 2
	la	$2, ___str___6_
# was:	la	_tmp_5_, ___str___6_
# ___str___6_: string " "
	ori	$16, $2, 0
# was:	ori	_writeArrayNumberres_2_, _tmp_5_, 0
# 	ori	2,_tmp_5_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	2, _writeArrayNumberres_2_, 0
	addi	$29, $29, 12
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function writeArray
writeArray:
	sw	$31, -4($29)
	sw	$19, -20($29)
	sw	$18, -16($29)
	sw	$17, -12($29)
	sw	$16, -8($29)
	addi	$29, $29, -24
	ori	$16, $2, 0
# was:	ori	_param_a_7_, 2, 0
	la	$2, ___str___11_
# was:	la	_tmp_10_, ___str___11_
# ___str___11_: string "{ "
# 	ori	_letBind_9_,_tmp_10_,0
# 	ori	2,_tmp_10_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	_arr_reg_14_, _param_a_7_, 0
	lw	$16, 0($2)
# was:	lw	_size_reg_13_, 0(_arr_reg_14_)
	ori	$4, $28, 0
# was:	ori	_letBind_12_, 28, 0
	sll	$3, $16, 2
# was:	sll	_tmp_23_, _size_reg_13_, 2
	addi	$3, $3, 4
# was:	addi	_tmp_23_, _tmp_23_, 4
	add	$28, $28, $3
# was:	add	28, 28, _tmp_23_
	sw	$16, 0($4)
# was:	sw	_size_reg_13_, 0(_letBind_12_)
	addi	$17, $4, 4
# was:	addi	_addr_reg_17_, _letBind_12_, 4
	ori	$18, $0, 0
# was:	ori	_i_reg_18_, 0, 0
	addi	$19, $2, 4
# was:	addi	_elem_reg_15_, _arr_reg_14_, 4
_loop_beg_19_:
	sub	$2, $18, $16
# was:	sub	_tmp_reg_21_, _i_reg_18_, _size_reg_13_
	bgez	$2, _loop_end_20_
# was:	bgez	_tmp_reg_21_, _loop_end_20_
	lw	$2, 0($19)
# was:	lw	_res_reg_16_, 0(_elem_reg_15_)
# 	ori	2,_res_reg_16_,0
	jal	writeArrayNumber
# was:	jal	writeArrayNumber, 2
# 	ori	_tmp_reg_22_,2,0
# 	ori	_res_reg_16_,_tmp_reg_22_,0
	addi	$19, $19, 4
# was:	addi	_elem_reg_15_, _elem_reg_15_, 4
	sw	$2, 0($17)
# was:	sw	_res_reg_16_, 0(_addr_reg_17_)
	addi	$17, $17, 4
# was:	addi	_addr_reg_17_, _addr_reg_17_, 4
	addi	$18, $18, 1
# was:	addi	_i_reg_18_, _i_reg_18_, 1
	j	_loop_beg_19_
_loop_end_20_:
	la	$2, ___str___25_
# was:	la	_tmp_24_, ___str___25_
# ___str___25_: string "}"
	ori	$16, $2, 0
# was:	ori	_writeArrayres_8_, _tmp_24_, 0
# 	ori	2,_tmp_24_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	2, _writeArrayres_8_, 0
	addi	$29, $29, 24
	lw	$19, -20($29)
	lw	$18, -16($29)
	lw	$17, -12($29)
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function testScanFun
testScanFun:
	sw	$31, -4($29)
	addi	$29, $29, -8
# 	ori	_param_x_26_,2,0
# 	ori	_param_y_27_,3,0
# 	ori	_plus_L_29_,_param_x_26_,0
# 	ori	_plus_R_30_,_param_y_27_,0
	add	$2, $2, $3
# was:	add	_testScanFunres_28_, _plus_L_29_, _plus_R_30_
# 	ori	2,_testScanFunres_28_,0
	addi	$29, $29, 8
	lw	$31, -4($29)
	jr	$31
# Function main
main:
	sw	$31, -4($29)
	sw	$20, -24($29)
	sw	$19, -20($29)
	sw	$18, -16($29)
	sw	$17, -12($29)
	sw	$16, -8($29)
	addi	$29, $29, -28
	ori	$2, $0, 4
# was:	ori	_size_reg_33_, 0, 4
	addi	$2, $2, -1
# was:	addi	_size_reg_33_, _size_reg_33_, -1
	bgez	$2, _safe_lab_34_
# was:	bgez	_size_reg_33_, _safe_lab_34_
	ori	$5, $0, 14
# was:	ori	5, 0, 14
	j	_IllegalArrSizeError_
_safe_lab_34_:
	addi	$2, $2, 1
# was:	addi	_size_reg_33_, _size_reg_33_, 1
	ori	$3, $28, 0
# was:	ori	_letBind_32_, 28, 0
	sll	$4, $2, 2
# was:	sll	_tmp_40_, _size_reg_33_, 2
	addi	$4, $4, 4
# was:	addi	_tmp_40_, _tmp_40_, 4
	add	$28, $28, $4
# was:	add	28, 28, _tmp_40_
	sw	$2, 0($3)
# was:	sw	_size_reg_33_, 0(_letBind_32_)
	addi	$5, $3, 4
# was:	addi	_addr_reg_35_, _letBind_32_, 4
	ori	$6, $0, 0
# was:	ori	_i_reg_36_, 0, 0
_loop_beg_37_:
	sub	$4, $6, $2
# was:	sub	_tmp_reg_39_, _i_reg_36_, _size_reg_33_
	bgez	$4, _loop_end_38_
# was:	bgez	_tmp_reg_39_, _loop_end_38_
	sw	$6, 0($5)
# was:	sw	_i_reg_36_, 0(_addr_reg_35_)
	addi	$5, $5, 4
# was:	addi	_addr_reg_35_, _addr_reg_35_, 4
	addi	$6, $6, 1
# was:	addi	_i_reg_36_, _i_reg_36_, 1
	j	_loop_beg_37_
_loop_end_38_:
	ori	$2, $0, 2
# was:	ori	_startExp_reg_47_, 0, 2
# 	ori	_arr_reg_43_,_letBind_32_,0
	lw	$16, 0($3)
# was:	lw	_size_reg_42_, 0(_arr_reg_43_)
	addi	$16, $16, 1
# was:	addi	_size_reg_42_, _size_reg_42_, 1
	ori	$17, $28, 0
# was:	ori	_letBind_41_, 28, 0
	sll	$4, $16, 2
# was:	sll	_tmp_56_, _size_reg_42_, 2
	addi	$4, $4, 4
# was:	addi	_tmp_56_, _tmp_56_, 4
	add	$28, $28, $4
# was:	add	28, 28, _tmp_56_
	sw	$16, 0($17)
# was:	sw	_size_reg_42_, 0(_letBind_41_)
	addi	$18, $17, 4
# was:	addi	_addr_reg_49_, _letBind_41_, 4
	ori	$19, $0, 0
# was:	ori	_i_reg_50_, 0, 0
	addi	$20, $3, 4
# was:	addi	_elem_reg_44_, _arr_reg_43_, 4
	ori	$3, $2, 0
# was:	ori	_res_reg_46_, _startExp_reg_47_, 0
	sb	$3, 0($18)
# was:	sb	_res_reg_46_, 0(_addr_reg_49_)
	addi	$18, $18, 4
# was:	addi	_addr_reg_49_, _addr_reg_49_, 4
	addi	$19, $19, 1
# was:	addi	_i_reg_50_, _i_reg_50_, 1
	lw	$3, 0($20)
# was:	lw	_res_reg_46_, 0(_elem_reg_44_)
# 	ori	2,_startExp_reg_47_,0
# 	ori	3,_res_reg_46_,0
	jal	testScanFun
# was:	jal	testScanFun, 23
# 	ori	_tmp_reg_51_,2,0
	ori	$3, $2, 0
# was:	ori	_res_reg_46_, _tmp_reg_51_, 0
	ori	$2, $3, 0
# was:	ori	_last_res_45_, _res_reg_46_, 0
	addi	$20, $20, 4
# was:	addi	_elem_reg_44_, _elem_reg_44_, 4
	j	_loop_fj_48_
_loop_beg_52_:
	sub	$3, $19, $16
# was:	sub	_tmp_reg_54_, _i_reg_50_, _size_reg_42_
	bgez	$3, _loop_end_53_
# was:	bgez	_tmp_reg_54_, _loop_end_53_
	lw	$3, 0($20)
# was:	lw	_res_reg_46_, 0(_elem_reg_44_)
# 	ori	2,_last_res_45_,0
# 	ori	3,_res_reg_46_,0
	jal	testScanFun
# was:	jal	testScanFun, 23
# 	ori	_tmp_reg_55_,2,0
	ori	$3, $2, 0
# was:	ori	_res_reg_46_, _tmp_reg_55_, 0
	ori	$2, $3, 0
# was:	ori	_last_res_45_, _res_reg_46_, 0
	addi	$20, $20, 4
# was:	addi	_elem_reg_44_, _elem_reg_44_, 4
_loop_fj_48_:
	sw	$3, 0($18)
# was:	sw	_res_reg_46_, 0(_addr_reg_49_)
	addi	$18, $18, 4
# was:	addi	_addr_reg_49_, _addr_reg_49_, 4
	addi	$19, $19, 1
# was:	addi	_i_reg_50_, _i_reg_50_, 1
	j	_loop_beg_52_
_loop_end_53_:
	ori	$2, $17, 0
# was:	ori	_arg_57_, _letBind_41_, 0
# 	ori	2,_arg_57_,0
	jal	writeArray
# was:	jal	writeArray, 2
# 	ori	_mainres_31_,2,0
# 	ori	2,_mainres_31_,0
	addi	$29, $29, 28
	lw	$20, -24($29)
	lw	$19, -20($29)
	lw	$18, -16($29)
	lw	$17, -12($29)
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
ord:
	jr	$31
chr:
	andi	$2, $2, 255
	jr	$31
putint:
	addi	$29, $29, -8
	sw	$2, 0($29)
	sw	$4, 4($29)
	ori	$4, $2, 0
	ori	$2, $0, 1
	syscall
	ori	$2, $0, 4
	la	$4, _space_
	syscall
	lw	$2, 0($29)
	lw	$4, 4($29)
	addi	$29, $29, 8
	jr	$31
getint:
	ori	$2, $0, 5
	syscall
	jr	$31
putchar:
	addi	$29, $29, -8
	sw	$2, 0($29)
	sw	$4, 4($29)
	ori	$4, $2, 0
	ori	$2, $0, 11
	syscall
	ori	$2, $0, 4
	la	$4, _space_
	syscall
	lw	$2, 0($29)
	lw	$4, 4($29)
	addi	$29, $29, 8
	jr	$31
dynalloc:
	ori	$4, $2, 0
	ori	$2, $0, 9
	syscall
	jr	$31
getchar:
	addi	$29, $29, -8
	sw	$4, 0($29)
	sw	$5, 4($29)
	ori	$2, $0, 12
	syscall
	ori	$5, $2, 0
	ori	$2, $0, 4
	la	$4, _cr_
	syscall
	ori	$2, $5, 0
	lw	$4, 0($29)
	lw	$5, 4($29)
	addi	$29, $29, 8
	jr	$31
putstring:
	addi	$29, $29, -16
	sw	$2, 0($29)
	sw	$4, 4($29)
	sw	$5, 8($29)
	sw	$6, 12($29)
	lw	$4, 0($2)
	addi	$5, $2, 4
	add	$6, $5, $4
	ori	$2, $0, 11
putstring_begin:
	sub	$4, $5, $6
	bgez	$4, putstring_done
	lb	$4, 0($5)
	syscall
	addi	$5, $5, 1
	j	putstring_begin
putstring_done:
	lw	$2, 0($29)
	lw	$4, 4($29)
	lw	$5, 8($29)
	lw	$6, 12($29)
	addi	$29, $29, 16
	jr	$31
_IllegalArrSizeError_:
	la	$4, _IllegalArrSizeString_
	ori	$2, $0, 4
	syscall
	ori	$4, $5, 0
	ori	$2, $0, 1
	syscall
	la	$4, _cr_
	ori	$2, $0, 4
	syscall
	j	_stop_
	.data	
_cr_:
	.asciiz	"\n"
_space_:
	.asciiz	" "
_IllegalArrSizeString_:
	.asciiz	"Error: Array size less or equal to 0 at line "
# String Literals
	.align	2
___str___25_:
	.space	4
	.asciiz	"}"
	.align	2
___str___11_:
	.space	4
	.asciiz	"{ "
	.align	2
___str___6_:
	.space	4
	.asciiz	" "
	.align	2
_true:
	.space	4
	.asciiz	"True"
	.align	2
_false:
	.space	4
	.asciiz	"False"
	.align	2
_heap_:
	.space	100000
