	.text	0x00400000
	.globl	main
	la	$28, _heap_
	la	$4, ___str___25_
# was:	la	___str___25__addr, ___str___25_
	ori	$3, $0, 1
# was:	ori	___str___25__init, 0, 1
	sw	$3, 0($4)
# was:	sw	___str___25__init, 0(___str___25__addr)
	la	$4, ___str___11_
# was:	la	___str___11__addr, ___str___11_
	ori	$3, $0, 2
# was:	ori	___str___11__init, 0, 2
	sw	$3, 0($4)
# was:	sw	___str___11__init, 0(___str___11__addr)
	la	$4, ___str___6_
# was:	la	___str___6__addr, ___str___6_
	ori	$3, $0, 1
# was:	ori	___str___6__init, 0, 1
	sw	$3, 0($4)
# was:	sw	___str___6__init, 0(___str___6__addr)
	la	$4, _true
# was:	la	_true_addr, _true
	ori	$3, $0, 4
# was:	ori	_true_init, 0, 4
	sw	$3, 0($4)
# was:	sw	_true_init, 0(_true_addr)
	la	$3, _false
# was:	la	_false_addr, _false
	ori	$4, $0, 5
# was:	ori	_false_init, 0, 5
	sw	$4, 0($3)
# was:	sw	_false_init, 0(_false_addr)
	jal	main
_stop_:
	ori	$2, $0, 10
	syscall
# Function writeArrayNumber
writeArrayNumber:
	sw	$31, -4($29)
	sw	$16, -8($29)
	addi	$29, $29, -12
# 	ori	_param_n_1_,2,0
# 	ori	_tmp_4_,_param_n_1_,0
# 	ori	_letBind_3_,_tmp_4_,0
# 	ori	2,_letBind_3_,0
	jal	putint
# was:	jal	putint, 2
	la	$2, ___str___6_
# was:	la	_tmp_5_, ___str___6_
# ___str___6_: string " "
	ori	$16, $2, 0
# was:	ori	_writeArrayNumberres_2_, _tmp_5_, 0
# 	ori	2,_tmp_5_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	2, _writeArrayNumberres_2_, 0
	addi	$29, $29, 12
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function writeArray
writeArray:
	sw	$31, -4($29)
	sw	$19, -20($29)
	sw	$18, -16($29)
	sw	$17, -12($29)
	sw	$16, -8($29)
	addi	$29, $29, -24
	ori	$16, $2, 0
# was:	ori	_param_a_7_, 2, 0
	la	$2, ___str___11_
# was:	la	_tmp_10_, ___str___11_
# ___str___11_: string "{ "
# 	ori	_letBind_9_,_tmp_10_,0
# 	ori	2,_tmp_10_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	_arr_reg_14_, _param_a_7_, 0
	lw	$16, 0($2)
# was:	lw	_size_reg_13_, 0(_arr_reg_14_)
	ori	$4, $28, 0
# was:	ori	_letBind_12_, 28, 0
	sll	$3, $16, 2
# was:	sll	_tmp_23_, _size_reg_13_, 2
	addi	$3, $3, 4
# was:	addi	_tmp_23_, _tmp_23_, 4
	add	$28, $28, $3
# was:	add	28, 28, _tmp_23_
	sw	$16, 0($4)
# was:	sw	_size_reg_13_, 0(_letBind_12_)
	addi	$17, $4, 4
# was:	addi	_addr_reg_17_, _letBind_12_, 4
	ori	$18, $0, 0
# was:	ori	_i_reg_18_, 0, 0
	addi	$19, $2, 4
# was:	addi	_elem_reg_15_, _arr_reg_14_, 4
_loop_beg_19_:
	sub	$2, $18, $16
# was:	sub	_tmp_reg_21_, _i_reg_18_, _size_reg_13_
	bgez	$2, _loop_end_20_
# was:	bgez	_tmp_reg_21_, _loop_end_20_
	lw	$2, 0($19)
# was:	lw	_res_reg_16_, 0(_elem_reg_15_)
# 	ori	2,_res_reg_16_,0
	jal	writeArrayNumber
# was:	jal	writeArrayNumber, 2
# 	ori	_tmp_reg_22_,2,0
# 	ori	_res_reg_16_,_tmp_reg_22_,0
	addi	$19, $19, 4
# was:	addi	_elem_reg_15_, _elem_reg_15_, 4
	sw	$2, 0($17)
# was:	sw	_res_reg_16_, 0(_addr_reg_17_)
	addi	$17, $17, 4
# was:	addi	_addr_reg_17_, _addr_reg_17_, 4
	addi	$18, $18, 1
# was:	addi	_i_reg_18_, _i_reg_18_, 1
	j	_loop_beg_19_
_loop_end_20_:
	la	$2, ___str___25_
# was:	la	_tmp_24_, ___str___25_
# ___str___25_: string "}"
	ori	$16, $2, 0
# was:	ori	_writeArrayres_8_, _tmp_24_, 0
# 	ori	2,_tmp_24_,0
	jal	putstring
# was:	jal	putstring, 2
	ori	$2, $16, 0
# was:	ori	2, _writeArrayres_8_, 0
	addi	$29, $29, 24
	lw	$19, -20($29)
	lw	$18, -16($29)
	lw	$17, -12($29)
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
# Function testfilterFun
testfilterFun:
	sw	$31, -4($29)
	addi	$29, $29, -8
	ori	$4, $2, 0
# was:	ori	_param_x_26_, 2, 0
# 	ori	_eq_L_30_,_param_x_26_,0
	ori	$3, $0, 2
# was:	ori	_eq_R_31_, 0, 2
	ori	$2, $0, 0
# was:	ori	_or_L_28_, 0, 0
	bne	$4, $3, _false_32_
# was:	bne	_eq_L_30_, _eq_R_31_, _false_32_
	ori	$2, $0, 1
# was:	ori	_or_L_28_, 0, 1
_false_32_:
# 	ori	_eq_L_33_,_param_x_26_,0
	ori	$3, $0, 1
# was:	ori	_eq_R_34_, 0, 1
	ori	$5, $0, 0
# was:	ori	_or_R_29_, 0, 0
	bne	$4, $3, _false_35_
# was:	bne	_eq_L_33_, _eq_R_34_, _false_35_
	ori	$5, $0, 1
# was:	ori	_or_R_29_, 0, 1
_false_35_:
	or	$2, $2, $5
# was:	or	_testfilterFunres_27_, _or_L_28_, _or_R_29_
# 	ori	2,_testfilterFunres_27_,0
	addi	$29, $29, 8
	lw	$31, -4($29)
	jr	$31
# Function main
main:
	sw	$31, -4($29)
	sw	$22, -32($29)
	sw	$21, -28($29)
	sw	$20, -24($29)
	sw	$19, -20($29)
	sw	$18, -16($29)
	sw	$17, -12($29)
	sw	$16, -8($29)
	addi	$29, $29, -36
	ori	$3, $0, 4
# was:	ori	_size_reg_38_, 0, 4
	addi	$3, $3, -1
# was:	addi	_size_reg_38_, _size_reg_38_, -1
	bgez	$3, _safe_lab_39_
# was:	bgez	_size_reg_38_, _safe_lab_39_
	ori	$5, $0, 14
# was:	ori	5, 0, 14
	j	_IllegalArrSizeError_
_safe_lab_39_:
	addi	$3, $3, 1
# was:	addi	_size_reg_38_, _size_reg_38_, 1
	ori	$2, $28, 0
# was:	ori	_letBind_37_, 28, 0
	sll	$4, $3, 2
# was:	sll	_tmp_45_, _size_reg_38_, 2
	addi	$4, $4, 4
# was:	addi	_tmp_45_, _tmp_45_, 4
	add	$28, $28, $4
# was:	add	28, 28, _tmp_45_
	sw	$3, 0($2)
# was:	sw	_size_reg_38_, 0(_letBind_37_)
	addi	$4, $2, 4
# was:	addi	_addr_reg_40_, _letBind_37_, 4
	ori	$5, $0, 0
# was:	ori	_i_reg_41_, 0, 0
_loop_beg_42_:
	sub	$6, $5, $3
# was:	sub	_tmp_reg_44_, _i_reg_41_, _size_reg_38_
	bgez	$6, _loop_end_43_
# was:	bgez	_tmp_reg_44_, _loop_end_43_
	sw	$5, 0($4)
# was:	sw	_i_reg_41_, 0(_addr_reg_40_)
	addi	$4, $4, 4
# was:	addi	_addr_reg_40_, _addr_reg_40_, 4
	addi	$5, $5, 1
# was:	addi	_i_reg_41_, _i_reg_41_, 1
	j	_loop_beg_42_
_loop_end_43_:
# 	ori	_arr_reg_48_,_letBind_37_,0
	lw	$16, 0($2)
# was:	lw	_size_reg_47_, 0(_arr_reg_48_)
	ori	$17, $28, 0
# was:	ori	_letBind_46_, 28, 0
	sll	$3, $16, 2
# was:	sll	_tmp_62_, _size_reg_47_, 2
	addi	$3, $3, 4
# was:	addi	_tmp_62_, _tmp_62_, 4
	add	$28, $28, $3
# was:	add	28, 28, _tmp_62_
	sw	$16, 0($17)
# was:	sw	_size_reg_47_, 0(_letBind_46_)
	addi	$18, $17, 4
# was:	addi	_addr_reg_53_, _letBind_46_, 4
	ori	$19, $0, 0
# was:	ori	_i_reg_54_, 0, 0
	addi	$20, $2, 4
# was:	addi	_elem_reg_49_, _arr_reg_48_, 4
	ori	$21, $0, 0
# was:	ori	_count_reg_52_, 0, 0
_loop_beg_55_:
	sub	$2, $19, $16
# was:	sub	_tmp_reg_58_, _i_reg_54_, _size_reg_47_
	bgez	$2, _loop_end_56_
# was:	bgez	_tmp_reg_58_, _loop_end_56_
	lw	$22, 0($20)
# was:	lw	_save_reg_51_, 0(_elem_reg_49_)
	lb	$22, 0($20)
# was:	lb	_save_reg_51_, 0(_elem_reg_49_)
	ori	$2, $22, 0
# was:	ori	2, _save_reg_51_, 0
	jal	testfilterFun
# was:	jal	testfilterFun, 2
# 	ori	_tmp_reg_59_,2,0
# 	ori	_res_reg_50_,_tmp_reg_59_,0
	addi	$20, $20, 4
# was:	addi	_elem_reg_49_, _elem_reg_49_, 4
	beq	$2, $0, _false_return_57_
# was:	beq	_res_reg_50_, 0, _false_return_57_
	addi	$21, $21, 1
# was:	addi	_count_reg_52_, _count_reg_52_, 1
	sw	$22, 0($18)
# was:	sw	_save_reg_51_, 0(_addr_reg_53_)
	addi	$18, $18, 4
# was:	addi	_addr_reg_53_, _addr_reg_53_, 4
_false_return_57_:
	addi	$19, $19, 1
# was:	addi	_i_reg_54_, _i_reg_54_, 1
	j	_loop_beg_55_
_loop_end_56_:
	sw	$21, 0($17)
# was:	sw	_count_reg_52_, 0(_letBind_46_)
	ori	$2, $17, 0
# was:	ori	_arg_63_, _letBind_46_, 0
# 	ori	2,_arg_63_,0
	jal	writeArray
# was:	jal	writeArray, 2
# 	ori	_mainres_36_,2,0
# 	ori	2,_mainres_36_,0
	addi	$29, $29, 36
	lw	$22, -32($29)
	lw	$21, -28($29)
	lw	$20, -24($29)
	lw	$19, -20($29)
	lw	$18, -16($29)
	lw	$17, -12($29)
	lw	$16, -8($29)
	lw	$31, -4($29)
	jr	$31
ord:
	jr	$31
chr:
	andi	$2, $2, 255
	jr	$31
putint:
	addi	$29, $29, -8
	sw	$2, 0($29)
	sw	$4, 4($29)
	ori	$4, $2, 0
	ori	$2, $0, 1
	syscall
	ori	$2, $0, 4
	la	$4, _space_
	syscall
	lw	$2, 0($29)
	lw	$4, 4($29)
	addi	$29, $29, 8
	jr	$31
getint:
	ori	$2, $0, 5
	syscall
	jr	$31
putchar:
	addi	$29, $29, -8
	sw	$2, 0($29)
	sw	$4, 4($29)
	ori	$4, $2, 0
	ori	$2, $0, 11
	syscall
	ori	$2, $0, 4
	la	$4, _space_
	syscall
	lw	$2, 0($29)
	lw	$4, 4($29)
	addi	$29, $29, 8
	jr	$31
dynalloc:
	ori	$4, $2, 0
	ori	$2, $0, 9
	syscall
	jr	$31
getchar:
	addi	$29, $29, -8
	sw	$4, 0($29)
	sw	$5, 4($29)
	ori	$2, $0, 12
	syscall
	ori	$5, $2, 0
	ori	$2, $0, 4
	la	$4, _cr_
	syscall
	ori	$2, $5, 0
	lw	$4, 0($29)
	lw	$5, 4($29)
	addi	$29, $29, 8
	jr	$31
putstring:
	addi	$29, $29, -16
	sw	$2, 0($29)
	sw	$4, 4($29)
	sw	$5, 8($29)
	sw	$6, 12($29)
	lw	$4, 0($2)
	addi	$5, $2, 4
	add	$6, $5, $4
	ori	$2, $0, 11
putstring_begin:
	sub	$4, $5, $6
	bgez	$4, putstring_done
	lb	$4, 0($5)
	syscall
	addi	$5, $5, 1
	j	putstring_begin
putstring_done:
	lw	$2, 0($29)
	lw	$4, 4($29)
	lw	$5, 8($29)
	lw	$6, 12($29)
	addi	$29, $29, 16
	jr	$31
_IllegalArrSizeError_:
	la	$4, _IllegalArrSizeString_
	ori	$2, $0, 4
	syscall
	ori	$4, $5, 0
	ori	$2, $0, 1
	syscall
	la	$4, _cr_
	ori	$2, $0, 4
	syscall
	j	_stop_
	.data	
_cr_:
	.asciiz	"\n"
_space_:
	.asciiz	" "
_IllegalArrSizeString_:
	.asciiz	"Error: Array size less or equal to 0 at line "
# String Literals
	.align	2
___str___25_:
	.space	4
	.asciiz	"}"
	.align	2
___str___11_:
	.space	4
	.asciiz	"{ "
	.align	2
___str___6_:
	.space	4
	.asciiz	" "
	.align	2
_true:
	.space	4
	.asciiz	"True"
	.align	2
_false:
	.space	4
	.asciiz	"False"
	.align	2
_heap_:
	.space	100000
