\select@language {english}
\select@language {english}
\contentsline {section}{\numberline {1}Task 1}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Parser}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}lexer}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Interpreter}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Type checker}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Code generator}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Test}{3}{subsection.1.6}
\contentsline {section}{\numberline {2}Task 2}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Parser}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Lexer}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Type checker}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Interpreter}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Code Generator}{5}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Tests}{7}{subsection.2.6}
\contentsline {section}{\numberline {3}Task 3}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Parser}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Lexer}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Type Checker}{7}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Interpreter}{8}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Code Generator}{8}{subsection.3.5}
\contentsline {section}{\numberline {4}Task 4}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Test}{9}{subsection.4.1}
